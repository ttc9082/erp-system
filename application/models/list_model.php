<?php
class list_model extends CI_Model{
public function __construct()
  {
    $this->load->database();
    $this->load->helper('url');
  }
  
  public function get_lists($page_num = 1, $num_per_page = 20){
    $this->db->order_by("purchase_date", "desc");
    $query = $this->db->get('hr_asset_norm', $num_per_page, ($page_num-1)*$num_per_page );
    return $query->result_array();
    }
    
  public function num_pages($num_per_page = 20){
    $query = $this->db->get('hr_asset_norm');
    $rows = $query->num_rows();
    $pages = ceil($rows/$num_per_page);
    return $pages;
    }
    
  public function get_units($id = FALSE){
  if ($id === FALSE)
  {
    $query = $this->db->get('hr_unit');
    return $query->result_array();
  }
  $query = $this->db->get_where('hr_unit', array('id' => $id));
  return $query->row_array();
  }
  
  public function set_asset(){
      $data = array(
          'asset_num' => $this->input->post('asset_num'),
          'unitid' => $this->input->post('unitid'),
          'humanname' => $this->input->post('humanname'),
          'type_desc' => $this->input->post('type_desc'),
          'brand' => $this->input->post('brand'),
          'model_num' => $this->input->post('model_num'),
          'serial_num' => $this->input->post('serial_num'),
          'memory_capacity' => $this->input->post('memory_capacity'),
          'disk_capacity' => $this->input->post('disk_capacity'),
          'purchase_date' => strtotime($this->input->post('purchase_date')),
          'receipt_date' => strtotime($this->input->post('receipt_date'))
                     );
      return $this->db->insert('hr_asset_norm', $data);
  }
  public function del($id=false){
      return $this->db->delete('hr_asset_norm', array('ID' => $id));
  }
  public function get_repair($id=false){
      if ($id === FALSE)
  {
    $query = $this->db->get('hr_asset_repair');
    return $query->result_array();
  }
  $query = $this->db->get_where('hr_asset_repair', array('asset_ID' => $id));
  return $query->result_array();
  }
  
  public function set_repair($id=false){
      $data = array(
          'asset_ID' => $id,
          'repair_date' => strtotime($this->input->post('repair_date')),
          'humanname' => $this->input->post('humanname'),
          'repair_depart' => $this->input->post('repair_depart'),
          'repair_tel' => $this->input->post('repair_tel'),
          'remark' => $this->input->post('remark'),
                     );
      return $this->db->insert('hr_asset_repair', $data);
  }
  
  public function get_assets($id = FALSE){
  if ($id === FALSE)
  {
    $query = $this->db->get('hr_asset_norm');
    return $query->result_array();
  }
  $query = $this->db->get_where('hr_asset_norm', array('id' => $id));
  return $query->row_array();
  }
  
  public function del_repair($id=false){
      return $this->db->delete('hr_asset_repair', array('ID' => $id));
  }
  
  public function del_transfer($id=false){
      return $this->db->delete('hr_asset_users', array('ID' => $id));
  }
  
  public function del_useless($id=false){
      return $this->db->delete('hr_asset_useless', array('ID' => $id));
  }
  
   public function del_unit($id=false){
      return $this->db->delete('hr_unit', array('ID' => $id));
  }
  
  public function get_transfer($id=false){
      if ($id === FALSE)
  {
    $query = $this->db->get('hr_asset_users');
    return $query->result_array();
  }
  $query = $this->db->get_where('hr_asset_users', array('asset_ID' => $id));
  return $query->result_array();
  }
  
  public function set_transfer($id=false){
      $data = array(
          'asset_ID' => $id,
          'take_date' => strtotime($this->input->post('take_date')),
          'humanname' => $this->input->post('humanname'),
          'have_agreement' => $this->input->post('have_agreement'),
          'is_new' => $this->input->post('is_new'),
          'remark' => $this->input->post('remark'),
                     );
      return $this->db->insert('hr_asset_users', $data);
  }
 
  
  public function update_user($id=false){
      $data = array(
          'unitid' => $this->input->post('unitid'),
          'humanname' => $this->input->post('humanname')
      );
      $this->db->where('ID', $id);
      $this->db->update('hr_asset_norm', $data);
  }
  
  public function set_useless($id){
      $data = array(
          'asset_ID'=>  $id,
          'useless_date' => strtotime($this->input->post('useless_date')),
          'applyhuman' => $this->input->post('applyhuman'),
          'agreehuman' => $this->input->post('agreehuman'),
          'remark' => $this->input->post('remark')
      );
      return $this->db->insert('hr_asset_useless', $data);
  }
  
  public function get_useless($id=false){
       if ($id === FALSE)
  {
    $query = $this->db->get('hr_asset_useless');
    return $query->result_array();
  }
  $query = $this->db->get_where('hr_asset_useless', array('asset_ID' => $id));
  return $query->result_array();
  }
  
  public function update_useless($id=false, $is='1'){
      $data = array(
          'is_useless' => $is,
      );
      $this->db->where('ID', $id);
      $this->db->update('hr_asset_norm', $data);
  }
  
  public function get_useless_from_id($id=false){
  $query = $this->db->get_where('hr_asset_useless', array('ID' => $id));
  return $query->row_array();
  }
  
  public function update_asset($id){
      $data = array(
          'type_desc' => $this->input->post('type_desc'),
          'brand' => $this->input->post('brand'),
          'model_num' => $this->input->post('model_num'),
          'serial_num' => $this->input->post('serial_num'),
          'memory_capacity' => $this->input->post('memory_capacity'),
          'disk_capacity' => $this->input->post('disk_capacity'),
          'purchase_date' => strtotime($this->input->post('purchase_date')),
          'receipt_date' => strtotime($this->input->post('receipt_date'))
                     );
             $this->db->where('ID', $id);
      return $this->db->update('hr_asset_norm', $data);
  }
  
    public function add_unit(){
      $data = array(
          'unitname' => $this->input->post('unitname'),
          'charge_person' => $this->input->post('charge_person'),
      );
      return $this->db->insert('hr_unit', $data);
  }
  
  
  public function search_att(){
      if ($this->input->post('asset_num')){
          $asset_num = $this->input->post('asset_num');
          $this->db->where('asset_num', $asset_num);
      }
      if ($this->input->post('unitid')){
          $unitid= $this->input->post('unitid');
          $this->db->where('unitid', $unitid);
      }
      if ($this->input->post('humanname')){
          $humanname = $this->input->post('humanname');
          $this->db->where('humanname', $humanname);
      }
      if ($this->input->post('purchase_date_start')&&$this->input->post('purchase_date_end')){
          $purchase_date_start = strtotime($this->input->post('purchase_date_start'));
          $purchase_date_end = strtotime($this->input->post('purchase_date_end'));
          $this->db->where('purchase_date >=', $purchase_date_start);
          $this->db->where('purchase_date <=', $purchase_date_end);
      }
      if ($this->input->post('receipt_date_start')&&$this->input->post('receipt_date_end')){
          $receipt_date_start = strtotime($this->input->post('receipt_date_start'));
          $receipt_date_end = strtotime($this->input->post('receipt_date_end'));
          $this->db->where('receipt_date >=', $receipt_date_start);
          $this->db->where('receipt_date <=', $receipt_date_end);
      }
      if ($this->input->post('mem_min')&&$this->input->post('mem_max')){
          $mem_min = $this->input->post('mem_min');
          $mem_max = $this->input->post('mem_max');
          $this->db->where('memory_capacity >=', $mem_min);
          $this->db->where('memory_capacity <=', $mem_max);
      }
      if ($this->input->post('model_num')){
          $model_num = $this->input->post('model_num');
          $this->db->where('model_num', $model_num);
      }
      if ($this->input->post('serial_num')){
          $serial_num = $this->input->post('serial_num');
          $this->db->where('serial_num', $serial_num);
      }
      $query = $this->db->get('hr_asset_norm');
      return $query->result_array();



  }
  
  
  public function search_rep(){
    $this->db->select('hr_asset_norm.*');
    $this->db->from('hr_asset_norm');
    $this->db->join('hr_asset_repair', 'hr_asset_norm.ID = hr_asset_repair.asset_ID');
      if ($this->input->post('humanname')){
          $humanname = $this->input->post('humanname');
          $this->db->where('hr_asset_repair.humanname', $humanname);
      }
      if ($this->input->post('repair_depart')){
          $repair_depart = $this->input->post('repair_depart');
          $this->db->where('hr_asset_repair.repair_depart', $repair_depart);
      }
      if ($this->input->post('repair_date_start')&&$this->input->post('repair_date_end')){
          $repair_date_start = strtotime($this->input->post('repair_date_start'));
          $repair_date_end = strtotime($this->input->post('repair_date_end'));
          $this->db->where('hr_asset_repair.repair_date >=', $repair_date_start);
          $this->db->where('hr_asset_repair.repair_date <=', $repair_date_end);
      }
      $query = $this->db->get();
      return $query->result_array();
  }
  
  
  public function search_trans(){
    $this->db->select('hr_asset_norm.*');
    $this->db->from('hr_asset_norm');
    $this->db->join('hr_asset_users', 'hr_asset_norm.ID = hr_asset_users.asset_ID');
      if ($this->input->post('humanname')){
          $humanname = $this->input->post('humanname');
          $this->db->where('hr_asset_users.humanname', $humanname);
      }
      if ($this->input->post('take_date_start')&&$this->input->post('take_date_end')){
          $take_date_start = strtotime($this->input->post('take_date_start'));
          $take_date_end = strtotime($this->input->post('take_date_end'));
          $this->db->where('hr_asset_users.take_date >=', $take_date_start);
          $this->db->where('hr_asset_users.take_date <=', $take_date_end);
      }
      $query = $this->db->get();
      return $query->result_array();
  }
  
  public function update_unit($id){
      $data = array(
          'unitname' => $this->input->post('unitname'),
          'charge_person' => $this->input->post('charge_person'),
      );
       $this->db->where('ID', $id);
      return $this->db->update('hr_unit', $data);
  }
  
}
?>
