<DIV CLASS="row">
<hr>
<footer>
<p>©2013</p>
</footer>
</div>
<script src="<?php echo base_url(); ?>/static/js/jq.js"></script>
<script src="<?php echo base_url(); ?>/static/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/static/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/static/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minView: 2,
        pickerPosition: "bottom-left"
    });
</script> 
</body>
</html>
