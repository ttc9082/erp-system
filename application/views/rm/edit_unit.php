<div class="row-fluid">
    <div class="span2">
        <ul class="nav nav-tabs nav-stacked">
            <li><a href="<?php echo base_url(); ?>/index.php/rm/index">资产信息列表</a></li>
            <li class><a href="<?php echo base_url();?>/index.php/rm/search">资产信息查询</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/add_new">新增资产信息</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/editunit">编辑部门信息</a></li>
        </ul>
    </div>
    <div class="span10">
        <legend>部门信息</legend>
        <?php if (validation_errors()):?>
                <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo validation_errors(); ?>
                </div>
        <?php endif; ?>
        <?php if ($message):?>
                <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo $message; ?>
                </div>
        <?php endif; ?>
        <table class="table table-bordered">
            <tr>
                <td>序号</td>
                <td>部门名称</td>
                <td>部门负责人</td>
                <td>操作</td>
            </tr>
            <?php $i = 1 ?>
            <?php foreach ($units as $unit):?>
            <?php echo form_open('index.php/rm/editunit/'.$unit['ID'] ) ?>
            <tr>
                <td><?php echo $i?></td>
                <td><input type="text" name="unitname" value="<?php echo $unit['unitname']?>" placeholder="部门名称"></td>
                <td><input type="text" name="charge_person" value="<?php echo $unit['charge_person']?>" placeholder="部门负责人"></td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-primary" type="submit" href="<?php echo base_url();?>index.php/rm/del_unit/'+id+'/'+'<?php echo strtr(uri_string(),'/','_'); ?>">更新</button>
                        <button class="btn btn-danger" type="button" href="javascript:void(0)" onclick="del(<?php echo $unit['ID'] ?>)">删除</button>
                    </div>
                </td>
            </tr>
        </form>
            <?php $i++ ?>
            <?php endforeach ?>
            <?php echo form_open('index.php/rm/editunit' ) ?>
            <tr>
                <td><?php echo $i?></td>
                <td><input type="text" name="unitname" value="" placeholder="新增部门名称"></td>
                <td><input type="text" name="charge_person" value="" placeholder="新增部门负责人"></td>
                <td><button class="btn btn-success" type="submit" >增加</button></td>
            </tr></form>   
        </table>
    </div>
</div>

<div id="del" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body">
    <p>确认删除此条目么？</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true" >关闭</a>
    <a href="#" class="btn btn-primary">确认</a>
  </div>
</div>
<script type="text/javascript">	function del(id){
    $('#del').modal('show').on('shown',function(){$(".btn-primary").attr('href','<?php echo base_url();?>index.php/rm/del_unit/'+id+'/'+'<?php echo strtr(uri_string(),'/','_'); ?>');})}</script>
                    