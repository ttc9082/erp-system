<div class="row-fluid">
    <div class="span2">
        <ul class="nav nav-tabs nav-stacked">
            <li><a href="<?php echo base_url(); ?>/index.php/rm/index">资产信息列表</a></li>
            <li class><a href="<?php echo base_url();?>/index.php/rm/search">资产信息查询</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/add_new">新增资产信息</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/editunit">编辑部门信息</a></li>
        </ul>
    </div>
    <div class="span10">
        <legend>固定资产信息列表</legend>
        <table class="table table-bordered">
            <tr class="info">
                <td>序号</td>
                <td>固定资产编号</td>
                <td>使用部门</td>
                <td>使用人员</td>
                <td>型号</td>
                <td>生产厂家</td>
                <td>P/N码</td>
                <td>S/N号</td>
                <td>内存(GB)</td>
                <td>硬盘(GB)</td>
                <td>采购日期</td>
                <td>发票日期</td>
                <td>详细操作</td>
            </tr>
            <?php $i = 1; ?>
            <?php foreach ($list as $item) :?>
            <tr <?php if ($item['is_useless'] == 1){echo 'class = "warning"'; }?>>
                <td><?php echo $i ?></td>
                <td><?php echo $item['asset_num']?></td>
                <td><?php echo $unitnames[$item['unitid']] ?></td>
                <td><?php echo $item['humanname'] ?></td>
                <td><?php echo $item['type_desc'] ?></td>
                <td><?php echo $item['brand'] ?></td>
                <td><?php echo $item['model_num'] ?></td>
                <td><?php echo $item['serial_num'] ?></td>
                <td><?php echo $item['memory_capacity'] ?></td>
                <td><?php echo $item['disk_capacity'] ?></td>
                <td><?php if ($item['purchase_date']){echo date("Y-m-d",$item['purchase_date']);} ?></td>
                <td><?php if ($item['receipt_date']){echo date("Y-m-d",$item['receipt_date']);} ?></td>
                <td>
                    <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="icon-cog"></i>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu">
                            <li><a href="<?php echo base_url() ?>/index.php/rm/edit/<?php echo $item['ID'] ?>">编辑信息</a></li>
                            <li><a href="<?php echo base_url() ?>/index.php/rm/addrepair/<?php echo $item['ID'] ?>">维修信息</a></li>
                            <li><a tabindex="-1" href="<?php echo base_url() ?>/index.php/rm/addtransfer/<?php echo $item['ID'] ?>">资产转移</a></li>
                            <li><a tabindex="-1" href="<?php echo base_url() ?>/index.php/rm/useless/<?php echo $item['ID'] ?>">登记报废</a></li>
                            <li class="divider"></li>
                            <li><a tabindex="-1" href="javascript:void(0)" onclick="del(<?php echo $item['ID'] ?>)">删除条目</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php $i++; endforeach;?>
        </table>
        <ul class="pager">
            <li class="previous <?php if ($page_num <= 1){echo 'disabled';}?>">
                <?php if ($page_num == 1 ): ?>
                <a>&larr; 上一页</a>
                <?php else: ?>
                <a href="<?php echo base_url(); ?>index.php/rm/index/<?php echo $page_num -1; ?>/<?php echo $num_per_page ;?>">&larr; 上一页</a>
                <?php endif ?>
            </li>
            <li class="next <?php if ($num_of_pages <= $page_num){echo 'disabled';}?>">
                <?php if ($num_of_pages <= $page_num): ?>
                <a>下一页 &rarr;</a>
                <?php else: ?>
                <a href="<?php echo base_url(); ?>index.php/rm/index/<?php echo $page_num +1; ?>/<?php echo $num_per_page ;?>">下一页 &rarr;</a>
                <?php endif ?>
            </li>
        </ul>
    </div>
</div>
<div id="del" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body">
    <p>确认删除此条目么？</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true" >关闭</a>
    <a href="#" class="btn btn-primary">确认</a>
  </div>
</div>



<script type="text/javascript">	function del(id){
    $('#del').modal('show').on('shown',function(){$(".btn-primary").attr('href','<?php echo base_url();?>index.php/rm/del/'+id+'/'+'<?php echo strtr(uri_string(),'/','_'); ?>');})}</script>



    


