<div class="row-fluid">
    <div class="span2">
        <ul class="nav nav-tabs nav-stacked">
            <li><a href="<?php echo base_url(); ?>/index.php/rm/index">资产信息列表</a></li>
            <li class><a href="<?php echo base_url();?>/index.php/rm/search">资产信息查询</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/add_new">新增资产信息</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/editunit">编辑部门信息</a></li>
        </ul>
    </div>
    <div class="span10">
        <?php echo form_open('index.php/rm/edit/'.$id ) ?>
        <legend>新增固定资产信息</legend>
        <?php if (validation_errors()):?>
                <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo validation_errors(); ?>
                </div>
        <?php endif; ?>
        <?php if ($message):?>
                <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo $message; ?>
                </div>
        <?php endif; ?>
        <table class="table table-bordered">
            <tr>
                <td><label>型号</label></td>
                <td><input type="text" name="type_desc" value="<?php echo $asset_row['type_desc']; ?>" placeholder="请输入型号"></td>
                <td><label>生产厂家</label></td>
                <td><input type="text" name="brand" value="<?php echo $asset_row['brand']; ?>" placeholder="请输入生产厂家"></td>
            </tr>
            <tr>
                <td><label>内存容量</label></td>
                <td><div class="input-append"><input class="span12" value="<?php echo $asset_row['memory_capacity']; ?>" type="text" name="memory_capacity" placeholder="请输入内存容量"><span class="add-on">GB</span></div></td>
                <td><label>硬盘容量</label></td>
                <td><div class="input-append"><input class="span12" value="<?php echo $asset_row['disk_capacity']; ?>" type="text" name="disk_capacity" placeholder="请输入硬盘容量"><span class="add-on">GB</span></div></td>
            </tr>
            <tr>
                <td><label>P/N码</label></td>
                <td><input type="text" name="model_num" value="<?php echo $asset_row['model_num']; ?>" placeholder="请输入P/N码"></td>
                <td><label>S/N码</label></td>
                <td><input type="text" name="serial_num" value="<?php echo $asset_row['serial_num']; ?>" placeholder="请输入S/N码"></td>
            </tr>
            <tr>
                <td><label>购买日期</label></td>
                <td>
                    <div class="input-append date form_datetime">
                        <input name="purchase_date" size="15" type="text" value="<?php if ($asset_row['purchase_date']){echo date("Y-m-d",$asset_row['purchase_date']);} ?>" readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                </td>
                <td><label>发票日期</label></td>
                <td><div class="input-append date form_datetime">
                        <input name="receipt_date" size="15" type="text" value="<?php if($asset_row['receipt_date']){echo date("Y-m-d",$asset_row['receipt_date']);} ?>" readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                </td>
            </tr>
        </table>
            <button class="btn btn-primary" type="submit">更新</button>

        </form>

    </div>
</div>