<div class="row-fluid">
    <div class="span2">
        <ul class="nav nav-tabs nav-stacked">
            <li><a href="<?php echo base_url(); ?>/index.php/rm/index">资产信息列表</a></li>
            <li class><a href="<?php echo base_url();?>/index.php/rm/search">资产信息查询</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/add_new">新增资产信息</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/editunit">编辑部门信息</a></li>
        </ul>
    </div>
        <div class="span10">
            <div class="row-fluid">
            <?php echo form_open('index.php/rm/addtransfer/'.$id) ?>
            <legend>新增转移信息</legend>
            <?php if (validation_errors()):?>
                <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
            <?php if ($message):?>
                    <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?php echo $message; ?>
                    </div>
            <?php endif; ?>
                <table class="table table-hover table-bordered table-striped">
                    <tr>
                        <td><label>领取时间</label></td>
                        <td>
                            <div class="input-append date form_datetime">
                            <input name="take_date" size="15" type="text" value="" placeholder="请点击选择日期。" readonly>
                            <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </td>
                        <td><label>所属部门</label></td>
                        <td>
                            <select name="unitid">
                                <option value="">请选择所属部门</option>
                                <?php foreach ($unit_list as $unit): ?>
                                <?php echo '<option value ='.$unit['ID'].'>'.$unit['unitname'].'</option>';?>
                                <?php endforeach ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label>领取人员</label></td>
                        <td><input type="text" name="humanname" placeholder="请输入领取人员的姓名。"></td>
                        <td><label>签署协议</label></td>
                        <td><select name ="have_agreement">
                                <option value="1" >是</option>
                                <option value="0" >否</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label>是否新机</label></td>
                        <td><select name ="is_new">
                                <option value="1" >是</option>
                                <option value="0" >否</option>
                            </select></td>
                        <td><label>备注</label></td>
                        <td colspan="3"><input class="span11" type="text" name="remark" placeholder="请输入备注。（选填）"></td>
                    </tr>
                </table>
                <button class="btn btn-primary" type="input">提交</button>
            </form>
        </div>
        <div class="row-fluid">
        <legend>资产转移信息 - <?php echo $asset_num; ?></legend>
                <table class="table table-hover table-bordered table-striped">
                    <tr>
                        <td class="span1">序号</td>
                        <td class="span2">领取人员</td>
                        <td class="span1">是否新机</td>
                        <td class="span2">领取时间</td>
                        <td class="span1">签署协议</td>
                        <td class="span3">备注</td>
                        <td class="span1">删除</td>
                    </tr>
                    <?php $i = 1; ?>
                    <?php foreach ($use_list as $use_item):?>
                    <tr>
                        <td class="span1"><?php echo $i;?></td>
                        <td class="span2"><?php echo $use_item['humanname'];?></td>
                        <td class="span1"><?php if($use_item['is_new']==1){echo '是';}else{echo '否';}?></td>
                        <td class="span2"><?php if ($use_item['take_date']){echo date("Y-m-d",$use_item['take_date']);}?></td>
                        <td class="span1"><?php if($use_item['have_agreement']==1){echo '是';}else{echo '否';} ?></td>
                        <td class="span3"><?php echo $use_item['remark']; ?></td>
                        <td class="span1"><button class="btn btn-danger" type="button" href="javascript:void(0)" onclick="del(<?php echo $use_item['ID'] ?>)">删除</button></td>
                    </tr>
                    <?php $i++; endforeach; ?>
                </table>
        </div>

    </div>
</div>

<div id="del" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body">
    <p>确认删除此条目么？</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true" >关闭</a>
    <a href="#" class="btn btn-primary">确认</a>
  </div>
</div>
<script type="text/javascript">	function del(id){
    $('#del').modal('show').on('shown',function(){$(".btn-primary").attr('href','<?php echo base_url();?>index.php/rm/del_transfer/'+id+'/'+'<?php echo strtr(uri_string(),'/','_'); ?>');})}</script>
                    
