<div class="row-fluid">
    <div class="span2 bs-docs-sidebar">
        <ul class="nav nav-tabs nav-stacked ">
            <li><a href="<?php echo base_url(); ?>/index.php/rm/index">资产信息列表</a></li>
            <li class><a href="<?php echo base_url();?>/index.php/rm/search">资产信息查询</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/add_new">新增资产信息</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/editunit">编辑部门信息</a></li>
        </ul>
    </div>
    <div class="span10">
        <div class="accordion" id="accordion2">
            <div class="accordion-group">
                <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                    通过资产信息查询
                </a>
                </div>
                <div id="collapseOne" class="accordion-body collapse in">
                <div class="accordion-inner">
                    <div class="well well-small">
                        <?php echo form_open('index.php/rm/search_att' ) ?>
                        <table class="table table-hover">
                            <tr>
                                <td class="span2">资产编号</td>
                                <td colspan="3"><input type="text" name ="asset_num" placeholder="资产编号查询"></td>
                            </tr>
                            <tr>
                                <td class="span2">使用情况</td>
                                <td colspan="2" class="span2">
                                    <select name="unitid">
                                    <option value="0">请选择部门</option>
                                    <?php foreach ($units as $unit): ?>
                                    <?php echo '<option value ='.$unit['ID'].'>'.$unit['unitname'].'</option>';?>
                                    <?php endforeach ?>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" name ="humanname" placeholder="请填写使用人员">
                                </td>
                            </tr>
                            <tr>
                                <td>购买日期</td>
                                <td colspan="2">
                                    <div class="input-append date form_datetime">
                                    <input name="purchase_date_start" size="10" type="text" value="" placeholder="请选择起始时间" readonly>
                                    <span class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-append date form_datetime">
                                    <input name="purchase_date_end" size="15" type="text" value="" placeholder="请选择结束时间" readonly>
                                    <span class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>发票日期</td>
                                <td colspan="2">
                                    <div class="input-append date form_datetime">
                                    <input name="receipt_date_start" size="15" type="text" value="" placeholder="请选择起始时间" readonly>
                                    <span class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-append date form_datetime">
                                    <input name="receipt_date_end" size="15" type="text" value="" placeholder="请选择结束时间" readonly>
                                    <span class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="span2">S/N</td>
                                <td colspan=""><input type="text" name ="serial_num" placeholder="S/N号查询"></td>
                                <td class="span2">P/N</td>
                                <td colspan=""><input type="text" name ="model_num" placeholder="P/N号查询"></td>
                            </tr>
                            <tr>
                                <td>
                                    内存区间
                                </td>
                                <td class="span2">
                                    <input type="text" name ="mem_min" class="span12" placeholder="最低内存大小">
                                </td>
                                <td class="span2">
                                    <input type="text" name ="mem_max" class="span12" placeholder="最高内存大小">
                                </td>
                                <td>
                                    <button class="btn btn-info" type="submit">查询</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                        </div>
                    </div>
                </div>
                </div>
            <div class="accordion-group">
                <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                    通过维修记录查询
                </a>
                </div>
                <div id="collapseTwo" class="accordion-body collapse">
                <div class="accordion-inner">
                    <div class="well well-small">
                        <?php echo form_open('index.php/rm/search_rep' ) ?>
                        <table class="table table-hover">
                            <tr>
                                <td>维修人员</td>
                                <td ><input type="text" name ="humanname" placeholder="请填维修人员姓名"></td>
                            </tr>
                            <tr>
                                <td >维修公司</td>
                                <td ><input type="text" name ="repair_depart" placeholder="请填维修公司名称"></td>
                            </tr>
                            <tr>
                                <td>维修日期</td>
                                <td >
                                    <div class="input-append date form_datetime">
                                    <input name="repair_date_start" size="15" type="text" value="" placeholder="请选择起始时间" readonly>
                                    <span class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                    <div class="input-append date form_datetime">
                                    <input name="repair_date_end" size="15" type="text" value="" placeholder="请选择结束时间" readonly>
                                    <span class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <button class="btn btn-info" type="submit">查询</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                        </div>
                </div>
                </div>
            </div>
        <div class="accordion-group">
                <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                    通过转移信息查询
                </a>
                </div>
                <div id="collapseThree" class="accordion-body collapse">
                <div class="accordion-inner">
                    <div class="well well-small">
                       <?php echo form_open('index.php/rm/search_trans' ) ?>
                        <table class="table table-hover">
                            <tr>
                                <td>转移人员</td>
                                <td ><input type="text" name ="humanname" placeholder="请填维修人员姓名"></td>
                            </tr>
                            <tr>
                                <td>转移时间</td>
                                <td >
                                    <div class="input-append date form_datetime">
                                    <input name="transfer_date_start" size="15" type="text" value="" placeholder="请选择起始时间" readonly>
                                    <span class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                    <div class="input-append date form_datetime">
                                    <input name="transfer_date_end" size="15" type="text" value="" placeholder="请选择结束时间" readonly>
                                    <span class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <button class="btn btn-info" type="submit">查询</button>
                                </td>
                            </tr>
                        </table>
                        </form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>