<div class="row-fluid">
    <div class="span2">
        <ul class="nav nav-tabs nav-stacked">
            <li><a href="<?php echo base_url(); ?>/index.php/rm/index">资产信息列表</a></li>
            <li class><a href="<?php echo base_url();?>/index.php/rm/search">资产信息查询</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/add_new">新增资产信息</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/editunit">编辑部门信息</a></li>
        </ul>
    </div>
    <div class="span10">
                <div class="row-fluid">
            <?php echo form_open('index.php/rm/addrepair/'.$id) ?>
            <legend>新增维修信息</legend>
            <?php if (validation_errors()):?>
                <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
            <?php if ($message):?>
                    <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?php echo $message; ?>
                    </div>
            <?php endif; ?>
                <table class="table table-hover table-bordered table-striped">
                    <tr>
                        <td><label>维修时间</label></td>
                        <td>
                            <div class="input-append date form_datetime">
                            <input name="repair_date" size="15" type="text" placeholder="请点击选择日期。" value="" readonly>
                            <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </td>
                        <td><label>报修人员</label></td>
                        <td><input type="text" name="humanname" placeholder="请填写维修人员名称。"></td>
                    </tr>
                    <tr>
                        <td><label>维修厂家</label></td>
                        <td><input type="text" name="repair_depart" placeholder="请填写厂家名称。"></td>
                        <td><label>维修电话</label></td>
                        <td><input type="text" name="repair_tel" placeholder="请填写维修联系电话。"></td>
                    </tr>
                    <tr>
                        <td><label>维修备注</label></td>
                        <td colspan="3"><input class="span11" type="text" name="remark" placeholder="请输入备注。（选填）"></td>
                    </tr>
                </table>
                <button class="btn btn-primary" type="input">提交</button>
            </form>
        </div>
        <div class="row-fluid">
        <legend>维修记录列表 - <?php echo $asset_num; ?></legend>
                <table class="table table-hover table-bordered table-striped">
                    <tr>
                        <td>序号</td>
                        <td>维修时间</td>
                        <td>报修人员</td>
                        <td>维修厂家</td>
                        <td>维修电话</td>
                        <td>备注</td>
                        <td>删除记录</td>
                    </tr>
                    <?php $i = 1; ?>
                    <?php foreach ($re_list as $re_item):?>
                    <tr>
                        <td class="span1"><?php echo $i;?></td>
                        <td class="span2"><?php if ($re_item['repair_date']){echo date("Y-m-d",$re_item['repair_date']);}?></td>
                        <td class="span2"><?php echo $re_item['humanname'];?></td>
                        <td class="span2"><?php echo $re_item['repair_depart'];?></td>
                        <td class="span1"><?php echo $re_item['repair_tel'];?></td>
                        <td class="span3"><?php echo $re_item['remark']; ?></td>
                        <td class="span1"><button class="btn btn-danger" type="button" href="javascript:void(0)" onclick="del(<?php echo $re_item['ID'] ?>)">删除</button></td>
                    </tr>
                    <?php $i++; endforeach; ?>
                </table>
        </div>
    </div>
</div>

<div id="del" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body">
    <p>确认删除此条目么？</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true" >关闭</a>
    <a href="#" class="btn btn-primary">确认</a>
  </div>
</div>
<script type="text/javascript">	function del(id){
    $('#del').modal('show').on('shown',function(){$(".btn-primary").attr('href','<?php echo base_url();?>index.php/rm/del_repair/'+id+'/'+'<?php echo strtr(uri_string(),'/','_'); ?>');})}</script>
                    