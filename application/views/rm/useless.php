<div class="row-fluid">
    <div class="span2">
        <ul class="nav nav-tabs nav-stacked">
            <li><a href="<?php echo base_url(); ?>/index.php/rm/index">资产信息列表</a></li>
            <li class><a href="<?php echo base_url();?>/index.php/rm/search">资产信息查询</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/add_new">新增资产信息</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/editunit">编辑部门信息</a></li>
        </ul>
    </div>
    <div class="span10">
                <div class="row-fluid <?php if ($useless_list){echo "hidden";}?>">
            <?php echo form_open('index.php/rm/useless/'.$id) ?>
            <legend class="">新增报废信息</legend>
            <?php if (validation_errors()):?>
                <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
            <?php if ($message):?>
                    <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?php echo $message; ?>
                    </div>
            <?php endif; ?>
                <table class="table table-hover table-bordered table-striped">
                    <tr>
                        <td><label>报废时间</label></td>
                        <td colspan="3">
                            <div class="input-append date form_datetime">
                            <input name="useless_date" size="15" type="text" placeholder="请点击选择日期。" value="" readonly>
                            <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><label>报废申报人</label></td>
                        <td><input type="text" name="applyhuman" placeholder="请填写报废申请人的名字。"></td>
                        <td><label>报废审核人</label></td>
                        <td><input type="text" name="agreehuman" placeholder="请填写报废审核人的名字。"></td>
                    </tr>
                    <tr>
                        <td><label>报废备注</label></td>
                        <td colspan="3"><input class="span11" type="text" name="remark" placeholder="请输入备注。（选填）"></td>
                    </tr>
                </table>
                <button class="btn btn-primary" type="input">提交</button>
            </form>
        </div>
        <div class="row-fluid">
        <legend>报废记录 - <?php echo $asset_num; ?></legend>
                <table class="table table-hover table-bordered table-striped">
                    <tr>
                        <td>固定资产编号</td>
                        <td>报废时间</td>
                        <td>报废申报人员</td>
                        <td>报废审核人员</td>
                        <td>备注</td>
                        <td>删除</td>
                    </tr>
                    <?php foreach ($useless_list as $useless_item):?>
                    <tr>
                        <td class="span2"><?php echo $asset_num;?></td>
                        <td class="span2"><?php if ($useless_item['useless_date']){echo date("Y-m-d",$useless_item['useless_date']);}?></td>
                        <td class="span2"><?php echo $useless_item['applyhuman'];?></td>
                        <td class="span2"><?php echo $useless_item['agreehuman'];?></td>
                        <td class="span3"><?php echo $useless_item['remark']; ?></td>
                        <td class="span1"><button class="btn btn-danger" type="button" href="javascript:void(0)" onclick="del(<?php echo $useless_item['ID'] ?>)">删除</button></td>
                    </tr>
                    <?php endforeach?>
                </table>
        </div>

    </div>
</div>

<div id="del" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body">
    <p>确认删除此条目么？</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true" >关闭</a>
    <a href="#" class="btn btn-primary">确认</a>
  </div>
</div>
<script type="text/javascript">	function del(id){
    $('#del').modal('show').on('shown',function(){$(".btn-primary").attr('href','<?php echo base_url();?>index.php/rm/del_useless/'+id+'/'+'<?php echo strtr(uri_string(),'/','_'); ?>');})}</script>