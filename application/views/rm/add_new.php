<div class="row-fluid">
    <div class="span2">
        <ul class="nav nav-tabs nav-stacked">
            <li><a href="<?php echo base_url(); ?>/index.php/rm/index">资产信息列表</a></li>
            <li class><a href="<?php echo base_url();?>/index.php/rm/search">资产信息查询</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/add_new">新增资产信息</a></li>
            <li><a href="<?php echo base_url(); ?>/index.php/rm/editunit">编辑部门信息</a></li>
        </ul>
    </div>
    <div class="span10">
        <?php echo form_open('index.php/rm/add_new') ?>
        <legend>编辑固定资产信息</legend>
        <?php if (validation_errors()):?>
                <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo validation_errors(); ?>
                </div>
        <?php endif; ?>
        <?php if ($message):?>
                <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo $message; ?>
                </div>
        <?php endif; ?>
        <table class="table table-bordered">
            <tr>
                <td><label>固定资产编号</label></td>
                <td><input type="text" name="asset_num" placeholder="要求必填、不与现有重复"></td>
            </tr>
            <tr>
                <td>使用部门</td>
                <td><select name="unitid">
                        <option value="">请选择所属部门</option>
                        <?php foreach ($unit_list as $unit): ?>
                        <?php echo '<option value ='.$unit['ID'].'>'.$unit['unitname'].'</option>';?>
                        <?php endforeach ?>
                    </select>
                </td>
                <td><label>使用人员</label></td>
                <td><input type="text" name="humanname" placeholder="请填写使用人员"></td>
            </tr>
            <tr>
                <td><label>型号</label></td>
                <td><input type="text" name="type_desc" placeholder="请填写型号"></td>
                <td><label>生产厂家</label></td>
                <td><input type="text" name="brand" placeholder="请填写品牌"></td>
            </tr>
            <tr>
                <td><label>内存容量</label></td>
                <td><div class="input-append"><input  class="span12" type="text" name="memory_capacity" placeholder="请填写内存容量（单位：GB）"><span class="add-on">GB</span></div></td>
                <td><label>硬盘容量</label></td>
                <td><div class="input-append"><input class="span12" type="text" name="disk_capacity" placeholder="请填写硬盘容量（单位：GB）"><span class="add-on">GB</span></div></td>
            </tr>
            <tr>
                <td><label>P/N码</label></td>
                <td><input type="text" name="model_num" placeholder="P/N码"></td>
                <td><label>S/N码</label></td>
                <td><input type="text" name="serial_num" placeholder="S/N码"></td>
            </tr>
            <tr>
                <td><label>购买日期</label></td>
                <td>
                    <div class="input-append date form_datetime">
                        <input name="purchase_date" size="15" type="text" value="" placeholder="请选择日期"readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                </td>
                <td><label>发票日期</label></td>
                <td><div class="input-append date form_datetime">
                        <input name="receipt_date" size="15" type="text" value="" placeholder="请选择日期"readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                </td>
            </tr>
        </table>
            <button class="btn btn-primary" type="submit">新增</button>

        </form>

    </div>
</div>