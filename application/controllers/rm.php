<?php
Class Rm extends CI_Controller{
     public function __construct()
  {
    parent::__construct();
    $this->load->model('list_model');
    $this->load->helper('url');
    $this->load->helper('form');
  }
    public function index($page_num = 1, $num_per_page = 20)
    {
        $unit_list= $this->list_model->get_units();
        foreach ($unit_list as $unit){
            $name_from_id[$unit['ID']] = $unit['unitname'];
        }
        
        $data['num_per_page'] = $num_per_page;
        $data['page_num'] = $page_num;
        $data['num_of_pages'] = $this->list_model->num_pages($num_per_page);
        $data['unitnames'] = $name_from_id;        
        $data['list'] = $this->list_model->get_lists($page_num, $num_per_page);
        $this->load->view('templates/header', $data);
        $this->load->view('rm/list', $data);
        $this->load->view('templates/footer', $data);   
    }
    public function add_new()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', '请填写 %s ！');
        $this->form_validation->set_message('is_unique', '%s 重复，请检查！');
        $this->form_validation->set_rules('asset_num', '固定资产编号', 'required|is_unique[hr_asset_norm.asset_num]');
        $this->form_validation->set_rules('purchase_date', '购买时间', 'required');
        $this->form_validation->set_rules('receipt_date', '发票时间', 'required');
        $this->form_validation->set_rules('unitid', '使用部门', 'required');

        
        
        $data['unit_list']= $this->list_model->get_units();
        
        
        
        if ($this->form_validation->run() === FALSE)
            {
                $data['message'] = '';
                $this->load->view('templates/header');
                $this->load->view('rm/add_new', $data);
                $this->load->view('templates/footer');
            }
        else
            {
                $data['message'] = '增加资产信息成功！';
                $this->list_model->set_asset();
                $this->load->view('templates/header');
                $this->load->view('rm/add_new', $data);
                $this->load->view('templates/footer');
            }
    }
    public function del($id=False, $origin_url){
        if(!$id || !$origin_url){
            $data['message'] = '删除失败！';
            $this->load->view('templates/header');
            $this->load->view('rm/wrong', $data);
            $this->load->view('templates/footer');
        }
            $red_url= strtr($origin_url,'_','/');
            $this->list_model->del($id);
            redirect('/index.php/'.$red_url, 'refresh');
    }

    public function addrepair($id){
        
        $asset_list = $this->list_model->get_assets($id);
        $data['asset_num'] = $asset_list['asset_num'];
        
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', '请填写 %s ！');
        $this->form_validation->set_message('is_unique', '%s 重复，请检查！');
        $this->form_validation->set_message('max_length', '%s 过长，请检查！');
        $this->form_validation->set_rules('repair_date', '维修时间', 'required');
        $this->form_validation->set_rules('repair_tel', '维修电话', 'max_length[12]');
        
        
        if ($this->form_validation->run() === FALSE)
            {   
                $data['id'] = $id;
                $data['re_list'] = $this->list_model->get_repair($id);
                $data['message'] = '';
                $this->load->view('templates/header');
                $this->load->view('rm/repair', $data);
                $this->load->view('templates/footer');
            }
        else
            {
                $data['id'] = $id;
                $data['message'] = '增加维修信息成功！';
                $this->list_model->set_repair($id);
                $data['re_list'] = $this->list_model->get_repair($id);
                $this->load->view('templates/header');
                $this->load->view('rm/repair', $data);
                $this->load->view('templates/footer');
            }
    }
    
     public function del_repair($id=False, $origin_url){
        if(!$id || !$origin_url){
            $data['message'] = '删除失败！';
            $this->load->view('templates/header');
            $this->load->view('rm/wrong', $data);
            $this->load->view('templates/footer');
        }
            $red_url= strtr($origin_url,'_','/');
            $this->list_model->del_repair($id);
            redirect('/index.php/'.$red_url, 'refresh');
    }
    public function del_transfer($id=False, $origin_url){
        if(!$id || !$origin_url){
            $data['message'] = '删除失败！';
            $this->load->view('templates/header');
            $this->load->view('rm/wrong', $data);
            $this->load->view('templates/footer');
        }
            $red_url= strtr($origin_url,'_','/');
            $this->list_model->del_transfer($id);
            redirect('/index.php/'.$red_url, 'refresh');
    }
     public function del_useless($id=False, $origin_url){
        if(!$id || !$origin_url){
            $data['message'] = '删除失败！';
            $this->load->view('templates/header');
            $this->load->view('rm/wrong', $data);
            $this->load->view('templates/footer');
        }
            $useless_row = $this->list_model->get_useless_from_id($id);
            $red_url= strtr($origin_url,'_','/');
            $this->list_model->del_useless($id);
            $this->list_model->update_useless($useless_row['asset_ID'],0);
            redirect('/index.php/'.$red_url, 'refresh');
    }
    
    public function addtransfer($id){
        
        $asset_list = $this->list_model->get_assets($id);
        $data['asset_num'] = $asset_list['asset_num'];
        $data['unit_list']= $this->list_model->get_units();
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', '请填写 %s ！');
        $this->form_validation->set_message('is_unique', '%s 重复，请检查！');
        $this->form_validation->set_rules('take_date', '领取时间', 'required');
        $this->form_validation->set_rules('humanname', '领取人员', 'required');
        
        
        
        if ($this->form_validation->run() === FALSE)
            {   
                $data['id'] = $id;
                $data['use_list'] = $this->list_model->get_transfer($id);
                $data['message'] = '';
                $this->load->view('templates/header');
                $this->load->view('rm/transfer', $data);
                $this->load->view('templates/footer');
            }
        else
            {
                $data['id'] = $id;
                $data['message'] = '增加转移信息成功！';
                $this->list_model->set_transfer($id);
                $this->list_model->update_user($id);
                $data['use_list'] = $this->list_model->get_transfer($id);
                $this->load->view('templates/header');
                $this->load->view('rm/transfer', $data);
                $this->load->view('templates/footer');
            }
    }
    
    public function useless($id){
        
        $asset_list = $this->list_model->get_assets($id);
        $data['asset_num'] = $asset_list['asset_num'];
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', '请填写 %s ！');
        $this->form_validation->set_message('is_unique', '%s 重复，请检查！');
        $this->form_validation->set_rules('useless_date', '报废时间', 'required');
        
      
        
        
        if ($this->form_validation->run() === FALSE)
            {   
                $data['id'] = $id;
                $data['useless_list'] = $this->list_model->get_useless($id);
                $data['message'] = '';
                $this->load->view('templates/header');
                $this->load->view('rm/useless', $data);
                $this->load->view('templates/footer');
            }
        else
            {
                $data['id'] = $id;
                $data['message'] = '登记报废成功！';
                $this->list_model->set_useless($id);
                $this->list_model->update_useless($id);
                $data['useless_list'] = $this->list_model->get_useless($id);
                $this->load->view('templates/header');
                $this->load->view('rm/useless', $data);
                $this->load->view('templates/footer');
            }
    }
    
    public function edit($id){
        $data['id'] = $id;
        $data['asset_row'] = $this->list_model->get_assets($id);
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', '请填写 %s ！');
        $this->form_validation->set_message('is_unique', '%s 重复，请检查！');
        $this->form_validation->set_rules('type_desc', '产品型号', 'required');
        $this->form_validation->set_rules('purchase_date', '购买时间', 'required');
        $this->form_validation->set_rules('receipt_date', '发票时间', 'required');
       
        if ($this->form_validation->run() === FALSE)
            {
                $data['message'] = '';
                $this->load->view('templates/header');
                $this->load->view('rm/edit', $data);
                $this->load->view('templates/footer');
            }
        else
            {
                $data['message'] = '更改资产信息成功！';
                $this->list_model->update_asset($id);
                $data['asset_row'] = $this->list_model->get_assets($id);
                $this->load->view('templates/header');
                $this->load->view('rm/edit', $data);
                $this->load->view('templates/footer');
            }
    }
     public function editunit($id = false){
                $data['units'] = $this->list_model->get_units();
                $this->load->library('form_validation');
                $this->form_validation->set_message('required', '请填写 %s ！');
                $this->form_validation->set_message('is_unique', '%s 重复，请检查！');
                $this->form_validation->set_rules('unitname', '部门名称', 'required');

                if ($this->form_validation->run() === FALSE)
                    {
                        $data['message'] = '';
                        $this->load->view('templates/header');
                        $this->load->view('rm/edit_unit', $data);
                        $this->load->view('templates/footer');
                    }
                else
                    {
                        if ($id == false){
                        $data['message'] = '新增资产信息成功！';
                        $this->list_model->add_unit();
                        $data['units'] = $this->list_model->get_units();
                        $this->load->view('templates/header');
                        $this->load->view('rm/edit_unit', $data);
                        $this->load->view('templates/footer');
                        }
                        else{
                            $data['message'] = '更新资产信息成功！';
                            $this->list_model->update_unit($id);
                            $data['units'] = $this->list_model->get_units();
                            $this->load->view('templates/header');
                            $this->load->view('rm/edit_unit', $data);
                            $this->load->view('templates/footer');
                        }
                    }
     }
     public function del_unit($id=False, $origin_url){
        if(!$id || !$origin_url){
            $data['message'] = '删除失败！';
            $this->load->view('templates/header');
            $this->load->view('rm/wrong', $data);
            $this->load->view('templates/footer');
        }
            $red_url= strtr($origin_url,'_','/');
            $this->list_model->del_unit($id);
            redirect('/index.php/'.$red_url, 'refresh');
    }
    
    public function search(){
        $data['units'] = $this->list_model->get_units();
        $data['message'] = '';
        $this->load->view('templates/header');
        $this->load->view('rm/search', $data);
        $this->load->view('templates/footer');
    }
    
    public function search_att(){
        $unit_list= $this->list_model->get_units();
        foreach ($unit_list as $unit){
            $name_from_id[$unit['ID']] = $unit['unitname'];
        }
        $data['unitnames'] = $name_from_id;  
        $data['list'] = $this->list_model->search_att();
        $this->load->view('templates/header', $data);
        $this->load->view('rm/result', $data);
        $this->load->view('templates/footer', $data); 
    }
    
    public function search_rep(){
        $unit_list= $this->list_model->get_units();
        foreach ($unit_list as $unit){
            $name_from_id[$unit['ID']] = $unit['unitname'];
        }
        $data['unitnames'] = $name_from_id;  
        $data['list'] = $this->list_model->search_rep();
        $this->load->view('templates/header', $data);
        $this->load->view('rm/result', $data);
        $this->load->view('templates/footer', $data); 
    }
    
       public function search_trans(){
        $unit_list= $this->list_model->get_units();
        foreach ($unit_list as $unit){
            $name_from_id[$unit['ID']] = $unit['unitname'];
        }
        $data['unitnames'] = $name_from_id;  
        $data['list'] = $this->list_model->search_trans();
        $this->load->view('templates/header', $data);
        $this->load->view('rm/result', $data);
        $this->load->view('templates/footer', $data); 
    }
}
?>
