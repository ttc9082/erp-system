CREATE TABLE `hr_asset_norm` (
`ID`  bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '系统标识，自增长，主键' ,
`asset_num`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司固定资产编号，手填，不允许重复' ,
`unitid`  int(11) NULL DEFAULT NULL COMMENT '部门标识' ,
`humanname`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员姓名，标识当前使用者，若转为库存，则登记为库存' ,
`type_desc`  char(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '笔记本型号' ,
`brand`  char(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '厂家' ,
`model_num`  char(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'P/N码' ,
`serial_num`  char(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'S/N号，产品序列号' ,
`memory_capacity`  char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内存容量（单位G）' ,
`disk_capacity`  char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '硬盘容量（单位G）' ,
`purchase_date`  int(10) NULL DEFAULT NULL COMMENT '采购日期' ,
`receipt_date`  int(10) NULL DEFAULT NULL COMMENT '发票日期' ,
`is_useless`  int(1) NULL DEFAULT 0 COMMENT '是否报废，1为是，0为否，默认值为0' ,
PRIMARY KEY (`ID`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=5
CHECKSUM=0
ROW_FORMAT=DYNAMIC
DELAY_KEY_WRITE=0
;
CREATE TABLE `hr_asset_repair` (
`ID`  bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '维修情况主键' ,
`asset_ID`  bigint(20) NULL DEFAULT NULL COMMENT '固定资产主键ID' ,
`repair_date`  int(10) NULL DEFAULT NULL COMMENT '维修日期' ,
`humanname`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系维修人员名称，即员工名称' ,
`repair_depart`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '维修厂家、商家名称' ,
`repair_tel`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '维修商家、厂家联系电话' ,
`remark`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '维修原因，维修情况记录等' ,
PRIMARY KEY (`ID`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1
CHECKSUM=0
ROW_FORMAT=DYNAMIC
DELAY_KEY_WRITE=0
;

CREATE TABLE `hr_asset_useless` (
`ID`  bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '报废情况主键，自增长' ,
`asset_ID`  bigint(20) NULL DEFAULT NULL COMMENT '固定资产主键ID' ,
`useless_date`  int(10) NULL DEFAULT NULL COMMENT '报废日期' ,
`applyhuman`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报废签署人请人' ,
`agreehuman`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报废签署人' ,
`remark`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注，用于填写报废原因等事项' ,
PRIMARY KEY (`ID`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1
CHECKSUM=0
ROW_FORMAT=DYNAMIC
DELAY_KEY_WRITE=0
;

CREATE TABLE `hr_asset_users` (
`ID`  bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '转移情况主键，自增长' ,
`asset_ID`  bigint(20) NULL DEFAULT NULL COMMENT '固定资产主键ID' ,
`humanname`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '领用人' ,
`is_new`  int(1) NULL DEFAULT NULL COMMENT '是否新机？默认为0,1为是，0为否' ,
`take_date`  int(10) NULL DEFAULT NULL COMMENT '领取时间' ,
`have_agreement`  int(1) NULL DEFAULT NULL COMMENT '是否签署协议，1为是，0为否' ,
`remark`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注' ,
PRIMARY KEY (`ID`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1
CHECKSUM=0
ROW_FORMAT=DYNAMIC
DELAY_KEY_WRITE=0
;

CREATE TABLE `hr_unit` (
`ID`  bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT ,
`unitname`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称' ,
`charge_person`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门负责人' ,
PRIMARY KEY (`ID`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=4
CHECKSUM=0
ROW_FORMAT=DYNAMIC
DELAY_KEY_WRITE=0
;




